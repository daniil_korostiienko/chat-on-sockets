'use strict';

angular
    .module('chatRoom', [])
    .service('chatService', function ($http) {
        this.getData = function () {
            return $http.get('/toDoList').then(function successCallback(response) {
                return response.data;
            }, function errorCallback(error) {
                console.log(error);
            });
        };

        this.addToDo = function (entry) {
            return $http.post('/toDoList', entry).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(error) {
                console.log(error);
            });
        };

        this.deleteDoc = function (entry) {
            return $http.delete('/toDoList/' + entry).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(error) {
                console.log(error);
            });
        };

        this.editDoc = function (entry) {
            return $http.put('/toDoList/' + entry._id, entry).then(function successCallback(response) {
                return response.data;
            }, function errorCallback(error) {
                console.log(error);
            });
        };
    })
    .controller('chatController', function ($scope, chatService) {
        var socket = io(),
            getUsername = function () {
                var local = localStorage.getItem('username');

                if (!local) {
                    localStorage.setItem('username', prompt('Enter your Username', 'Little gay'));
                    local = localStorage.getItem('username');
                }

                return local;
            },
            username = getUsername(),
            addMessage = function (msg) {
                var el;

                if (msg.user === username) {
                    el = $('<li>').addClass('user').text(msg.message + ':' + msg.user);
                } else if (msg.user === 'System') {
                    el = $('<li>').addClass('system').text(msg.message);
                } else {
                    el = $('<li>').addClass('contact').text(msg.user + ': ' + msg.message);
                }

                $('#messages').append(el);
            };

        $scope.username = getUsername();
        $scope.msgs = $scope.msgs || [];
        $scope.currentMessage = {
            user: $scope.username,
            message: ''
        };
        $scope.addMessage = function (msg) {
            
            

            socket.emit('chat message', msg);
            $scope.currentMessage = {
                user: $scope.username,
                message: ''
            };
        };

        socket.on('receive messages', function (msgs) {
            console.log(msgs);

            $scope.msgs = msgs;
            
            
            var messages = document.getElementById("messages");
            messages.scrollTop = messages.scrollHeight;
            
            for ( var i = 0 ; i < msgs.length ; i++){
                if ( msgs[i].user !== $scope.username && msgs[i].user !== 'System') {
                    $scope.contact = msgs[i].user;
                    break;
                }
                
            }
            $scope.$digest();
        });
    
        

        socket.on('chat message', function (msg) {
            var messages = document.getElementById("messages");

            $scope.msgs.push(msg);
            $scope.$digest();

            messages.scrollTop = messages.scrollHeight;
        });

        socket.emit('connectedUser', username);
    });

    
    