chatApp.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/chat");

    $stateProvider
        .state('chat', {
            url: "/chat",
            templateUrl: "../html/chatRoom.html",
            controller: 'chatController'
        });
//        .state('state1.list', {
//            url: "/list",
//            templateUrl: "partials/state1.list.html",
//            controller: function($scope) {
//                $scope.items = ["A", "List", "Of", "Items"];
//            }
//        });
});