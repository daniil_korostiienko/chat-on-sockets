var express = require('express'),
    fs = require('fs'),
    app = express(),
    http = require('http').Server(app),
    io = require('socket.io')(http),
    bodyParser = require('body-parser'),
    getMessages = function () {
        fs.readFile('message.txt', function (err, data) {
            if (err) throw err;
            if (data[0] === 44) {
                data = data.slice(1, data.length);
            }

            msgs = JSON.parse('[' + data + ']');
        });
    },
    saveMessage = function (msg) {
        fs.appendFile('message.txt', ',' + JSON.stringify(msg), function (err) {
            if (err) throw err;
            console.log(JSON.stringify(msg) + ' was appended to file!');
        });
    },
    msgs = getMessages() || [];

app.get('/', function (req, res) {
    res.sendfile('index.html');
});

app.use('/html', express.static(__dirname + '/src/html'));
app.use('/js', express.static(__dirname + '/src/js'));
app.use('/css', express.static(__dirname + '/src/css'));
app.use('/lib', express.static(__dirname + '/bower_components'));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

io.on('connection', function (socket) {
    var username = '';

    socket.on('connectedUser', function (user) {
        var message = {
            user: 'System',
            message: user + ' connected to channel'
        };

        username = user;
        console.log(username + ' connected');
        msgs.push(message);
        saveMessage(message);
        io.emit('chat message', message);
    });

    socket.on('disconnect', function(){
        console.log(username + ' disconnected');
        var message = {
            user: 'System',
            message: username + ' disconnected from channel'
        };

        msgs.push(message);
        saveMessage(message);
        io.emit('chat message', message);
    });

    io.emit('receive messages', msgs);

    socket.on('chat message', function (msg) {
        msgs.push(msg);
        console.log(msg);
        console.log(msgs);
        saveMessage(msg);
        io.emit('chat message', msg);
    });
});

app.get('*', function (request, response) {
    function isRest () {
        var notRest = ['OData', '.css', '.js', '.map', '.eot', '.ttf', '.svg', '.woff'],
            rest = true;

        notRest.forEach(function (key) {
            if (request.url.indexOf(key) !== -1) {
                rest = false;
            }
        });

        return rest;
    }

    if (isRest()) {
        response.sendFile('/index.html', { root: __dirname });
    }
});

http.listen(3000, function () {
    console.log('listening on *:3000');
});